# Laravel 2 Step Verification

[![Latest Stable Version](https://poser.pugx.org/jeremykenedy/laravel2step/v/stable)](https://packagist.org/packages/jeremykenedy/laravel2step)
[![Total Downloads](https://poser.pugx.org/jeremykenedy/laravel2step/downloads)](https://packagist.org/packages/jeremykenedy/laravel2step)
[![License](https://poser.pugx.org/jeremykenedy/laravel2step/license)](https://packagist.org/packages/jeremykenedy/laravel2step)

- [About](#about)
- [Future](#future)
- [License](#license)

### About
Laravel 2 step verifcation is a package to add 2 step user authentication to any laravel project easily. It is configurable and customizable. It uses notifications to send the user an email with a 4 digit verfication code.

### Future
* Have more configurable options via the config file to run to env file. For:
    * Optional compiled CSS/JS
    * Optional use of modals/alerts in front end with optional sweetalert2.js
    * Configurable blade extensions options.
    * Its own HTML email template.
* Add in additional notications for SMS or ???.
* Add in capture Ip Address.
* Change to incrimential tables and logic accordingly
    * Create Artisan command and job to prune said entries.

### License
Laravel-monitor is licensed under the MIT license. Enjoy!

